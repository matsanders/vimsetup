#!/bin/bash
#delete Clang files in project dirrectory and subdirectories

#check argument is directory and not link
dir=$(readlink -m "$1")
echo "[script:$0] args: $@"
if [ ! -d "$dir" ] || [ -L "$dir" ]; then
  echo "[ERROR:$0] argument should be directory"
  exit 1
fi

#check that directory is not part of another project
echo "remove clang build files..."
find "$dir" -name "\.clang_complete" -type f -exec rm -rf {} \;
echo "clang build files are removed!"
