#!/bin/bash
#collect all data from .clang_complete files in subdirectories into .clang_complete
#in current directory. Very helpfulwhen when using cmake and cc_args.py

#check argument is directory and not link
dir=$(readlink -m "$1")
echo "[script:$0] args: $@"
if [ ! -d "$dir" ] || [ -L "$dir" ]; then
  echo "[ERROR:$0] arguments should be directory"
  exit 1
fi

#initialize project
ccbs_rm_clang_build_files.sh $dir
ccbs_rm_cmake_build_files.sh $dir
C="${HOME}/.vim/bundle/clang_complete/boundle/clang_complete/bin/cc_args.py g++" \
CXX="${HOME}/.vim/bundle/clang_complete/bin/cc_args.py g++" cmake $@
make 
ccbs_rm_cmake_build_files.sh $dir
