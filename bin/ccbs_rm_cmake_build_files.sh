#!/bin/bash
#delete CMake files in current dirrectory and subdirectory
#check argument is directory and not link
dir=$(readlink -m "$1")
echo "[script:$0] args: $@"
if [ ! -d "$dir" ] || [ -L "$dir" ]; then
  echo "[ERROR:$0] argument $0 should be directory"
  exit 1
fi

cmake_file="$dir/CMakeLists.txt"
if [ ! -f "$cmake_file" ] || [ -L "$cmake_file" ]; then
  echo "[ERROR:$0] $dir is not CMake directory"
  exit 2
fi

make clean
find $dir -name \CMakeCache.txt -type f -print0 | xargs -0 rm -f
find $dir -name \cmake_install.cmake -type f -print0 | xargs -0 rm -f
find $dir -name \Makefile -type f -print0 | xargs -0 rm -f
find $dir -name CMakeFiles -type d -print0 | xargs -0 rm -rf
